from operator import index
import re
import glob
import numpy as np
import pandas as pd

#We import all the csv's
files = glob.glob('./data/*.csv')

#Crate a list with the names of the columns tha we do not need

for _ in files:
    if bool(re.search(r'2021-01Holo.csv', _)):
        df = pd.read_csv(_)
        df = df.filter(['name', 'iccid', 'SC / MC ', 'MB', 'Month/Year'])
        df = df.rename(columns={'name':'Name','iccid':'ICCID'})
        df.to_csv('./data/filtered-data/2021-01Holo.csv', index=False)
    elif bool(re.search(r'2021-02Holo.csv', _)):
        df = pd.read_csv(_)
        df = df.filter(['name', 'iccid', 'SC / MC ', 'MB', 'Month/Year'])
        df = df.rename(columns={'name':'Name','iccid':'ICCID'})
        df.to_csv('./data/filtered-data/2021-02Holo.csv', index=False)
    elif bool(re.search(r'2021-03Holo.csv', _)):
        df = pd.read_csv(_)
        df = df.filter(['Name', 'ICCID', 'SC / MC ', 'M3 Data (MB)', 'Month/Year'])
        df = df.rename(columns={'M3 Data (MB)':'MB'})
        df.to_csv('./data/filtered-data/2021-03Holo.csv', index=False)
    elif bool(re.search(r'2021-04Holo.csv', _)):
        df = pd.read_csv(_)
        df = df.filter(['Name', 'ICCID', 'SC / MC ', 'M4 Data (MB)', 'Month/Year'])
        df = df.rename(columns={'M4 Data (MB)':'MB'})
        df.to_csv('./data/filtered-data/2021-04Holo.csv', index=False)
    elif bool(re.search(r'2021-05Holo.csv', _)):
        df = pd.read_csv(_)
        df = df.filter(['Name', 'ICCID', 'SC / MC ', 'M5 Data (MB)', 'Month/Year'])
        df = df.rename(columns={'M5 Data (MB)':'MB'})
        df.to_csv('./data/filtered-data/2021-05Holo.csv', index=False)
    elif bool(re.search(r'2021-06Holo.csv', _)):
        df = pd.read_csv(_)
        df = df.filter(['Name', 'ICCID', 'SC / MC ', 'M6 Data (MB)', 'Month/Year'])
        df = df.rename(columns={'M6 Data (MB)':'MB'})
        df.to_csv('./data/filtered-data/2021-06Holo.csv', index=False)
    elif bool(re.search(r'2021-07Holo.csv', _)):
        df = pd.read_csv(_)
        df = df.filter(['Name', 'ICCID', 'SC / MC ', 'M7 Data (MB)', 'Month/Year'])
        df = df.rename(columns={'M7 Data (MB)':'MB'})
        df.to_csv('./data/filtered-data/2021-07Holo.csv', index=False)
    elif bool(re.search(r'2021-08Holo.csv', _)):
        df = pd.read_csv(_)
        df = df.filter(['Name', 'ICCID', 'SC / MC ', 'M8 Data (MB)', 'Month/Year'])
        df = df.rename(columns={'M8 Data (MB)':'MB'})
        df.to_csv('./data/filtered-data/2021-08Holo.csv', index=False)
    elif bool(re.search(r'2021-09Holo.csv', _)):
        df = pd.read_csv(_)
        df = df.filter(['Name', 'ICCID', 'SC / MC ', 'M9 Data (MB)', 'Month/Year'])
        df = df.rename(columns={'M9 Data (MB)':'MB'})
        df.to_csv('./data/filtered-data/2021-09Holo.csv', index=False)
    elif bool(re.search(r'2021-10Holo.csv', _)):
        df = pd.read_csv(_)
        df = df.filter(['Name', 'ICCID', 'SC / MC ', 'M10 Data (MB)', 'Month/Year'])
        df = df.rename(columns={'M10 Data (MB)':'MB'})
        df.to_csv('./data/filtered-data/2021-10Holo.csv', index=False)
    elif bool(re.search(r'2021-11Holo.csv', _)):
        df = pd.read_csv(_)
        df = df.filter(['Name', 'ICCID', 'SC / MC ', 'M11 Data (MB)', 'Month/Year'])
        df = df.rename(columns={'M11 Data (MB)':'MB'})
        df.to_csv('./data/filtered-data/2021-11Holo.csv', index=False)
    elif bool(re.search(r'2021-12Holo.csv', _)):
        df = pd.read_csv(_)
        df = df.filter(['Name', 'ICCID', 'SC / MC ', 'M12 Data (MB)', 'Month/Year'])
        df = df.rename(columns={'M12 Data (MB)':'MB'})
        df.to_csv('./data/filtered-data/2021-12Holo.csv', index=False)
    elif bool(re.search(r'2022-01Holo.csv', _)):
        df = pd.read_csv(_)
        df = df.filter(['Name', 'ICCID', 'SC / MC ', 'M1 Data (MB)', 'Month/Year'])
        df = df.rename(columns={'M1 Data (MB)':'MB'})
        df.to_csv('./data/filtered-data/2022-01Holo.csv', index=False)
    elif bool(re.search(r'2022-02Holo.csv', _)):
        df = pd.read_csv(_)
        df = df.filter(['Name', 'ICCID', 'SC / MC ', 'M2 Data (MB)', 'Month/Year'])
        df = df.rename(columns={'M2 Data (MB)':'MB'})
        df.to_csv('./data/filtered-data/2022-02Holo.csv', index=False)

    else:
        print('nothing to do here pal')


filtered_files = glob.glob('./data/filtered-data/*.csv')

df_list = []
for _ in filtered_files:
    csv = pd.read_csv(_)
    df_list.append(csv)

data = pd.concat(df_list)
data['Month/Year'] = pd.to_datetime(data['Month/Year'])


data.to_csv('./data/merged_data.csv', index=False)